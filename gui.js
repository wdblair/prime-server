var primes = (function () {
    var start, next, stop;

    prime = document.getElementById("prime");
    start = document.getElementById("start");
    next = document.getElementById("next");
    stop = document.getElementById("stop");
    
    return {
        id: 0,
        start: function () {
            $.get("primes.php", {action: "start"}).done(function (data) {
                primes.id = data;
                start.disabled = true;
                next.disabled = false;
                stop.disabled = false;
            });
        },
        next: function () {
            if (!primes.id) {
                alert("An error has occurred. Please try again later.");
                return;
            }
            
            $.get("primes.php", {action: "next", id: primes.id}).done(function (data) {
                prime.textContent = data;
            });
        },
        stop: function () {
            $.get("primes.php", {action: "stop", id: primes.id}).done(function (data) {
                primes.id = 0;
                start.disabled = false;
                next.disabled = true;
                stop.disabled = true;
                prime.textContent = "";
            });
        }
    }
})();
