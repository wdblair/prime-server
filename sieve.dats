(**
  A single-threaded server for serving prime numbers.
*)

#include
"share/atspre_define.hats"
#include
"share/atspre_staload.hats"

(* ****** ****** *)

staload UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

staload LinMap = "libats/SATS/linmap_avltree.sats"
staload _ = "libats/DATS/linmap_avltree.dats"

vtypedef map(k:t@ype, v:vt@ype) = $LinMap.map (k,v)

(* ****** ****** *)

%{^
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include <linux/limits.h>

#include <assert.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
%}

abst@ype socket = int

extern
fun establish_server (pathname: string): socket = "ext#"

extern
fun wait_for_request (socket): socket = "ext#"

(**
  Assume a message is a line of text
*)
extern
fun socket_recv_length (socket, int): string = "ext#"

overload .recv with socket_recv_length

extern
fun socket_send (socket, string): void = "ext#"

overload .send with socket_send

extern
fun socket_close (socket): void = "ext#"

overload .close with socket_close

(* ****** ****** *)

%{
  static struct sockaddr_un serveraddr;
 
  int establish_server (void *p) {
    char *path = (char *)p;
    int socket_fd;
    const int backlog = 5;

    srandom((unsigned int)time(NULL));

    /**
       First, check to see if the socket file already
       exists.
    */
    
    socket_fd = socket(PF_UNIX, SOCK_STREAM, 0);
    
    if (socket_fd < 0) {
      fprintf(stderr, "socket() failed\n");
      exit(1);
    }
    
    memset(&serveraddr, 0, sizeof(struct sockaddr_un));
    serveraddr.sun_family = AF_UNIX;
    snprintf(serveraddr.sun_path, PATH_MAX, path);
    
    /** give lenient permissions for 
        the socket and then bind. */
    umask(0);
    
    if(bind(socket_fd,
            (struct sockaddr *)&serveraddr,
            sizeof(struct sockaddr_un)) != 0) {
      fprintf(stderr, "bind() failed\n");
      exit(1);
    }
    
    if(listen(socket_fd, backlog) != 0) {
      fprintf(stderr, "listen() failed\n");
      return 1;
    }

    umask(S_IWGRP | S_IWOTH);
    
    return socket_fd;
  }

  int wait_for_request(int server_fd) {
    int connection_fd;
    
    while(1) {
      int connection_fd;
      int serveraddr_length = sizeof(serveraddr);
      
      if((connection_fd =
          accept(server_fd,
                 (struct sockaddr*)&serveraddr,
                 &serveraddr_length)) == -1) {
        fprintf(stderr, "accept error: %s\n", strerror(errno));
        break;
      }

      return connection_fd;
    }
  }

#define DEFAULTLEN 20

  void *socket_recv_length (int socket_fd, int len) {
    char buf[DEFAULTLEN];
    int nbytes;
    
    if (!(len < DEFAULTLEN)) {
      fprintf(stderr, "Length longer than %d not supported yet.", DEFAULTLEN);
      exit(1);
    }

    nbytes = read(socket_fd, buf, len);

    if (nbytes == 0) {
      return "";
    }
    
    if(nbytes == -1) {
      printf("Error reading: %s\n", strerror(errno));
      exit(1);
    }
    
    buf[nbytes] = '\0';
    
    char *msg = malloc(nbytes+1);
    strncpy(msg, buf, nbytes+1);
    
    return (void*)msg;
  }

  void socket_send (int socket_fd, void* m) {
    char *msg = (char *)m;
    int nbytes;
    int len = strlen(msg);
    
    nbytes = write(socket_fd, msg, len);
    
    if(nbytes == -1) {
      printf("Error writing: %s\n", strerror(errno));
      exit(1);
    }
    
    return;
  }

  void socket_close (int socket_fd) {
    close(socket_fd);
  }
%}

(* ****** ****** *)

vtypedef intstream = stream_vt int

extern
fn primes (): intstream

(* ****** ****** *)

#define :: stream_vt_cons
#define cons stream_vt_cons
#define nil stream_vt_nil

(* ****** ****** *)

staload "libc/SATS/stdlib.sats"

fun
eventloop (sock: socket): void = let
  var primemap: map(int, intstream) = $LinMap.linmap_nil()
  
  fun loop (primemap: &map(int, intstream) >> _?): void = let
    val req = wait_for_request (sock)
    val msg = req.recv (1)
  in
    case+ msg of
      | "s" => let
        // val () = prerrln! ("Starting stream!")
        
        fun findkey (
          primemap: !map(int, intstream)
        ): int = let
          val key = $UN.cast{int}(random())
          val p = $LinMap.linmap_search_ref(primemap, key)
        in
          if iseqz(p) then
            key
          else
            findkey (primemap)
        end
        
        val key = findkey (primemap)
        val sieve = primes ()
        
        var res: intstream?
        
        val found = $LinMap.linmap_insert (primemap, key, sieve, res)
        
        val () = (
          if found then {
            (**
             This shouldn't happen, but handle it anyways.
            *)
            prval () = opt_unsome (res)
            val () = stream_vt_free (res)
          }
          else {
            prval () = opt_unnone (res)
          }
        ): void
        
        val id = g0int2string (key)
        val id = $UN.castvwtp0{string} (id)
        
        val () = req.send (id)
        
        val id = $UN.castvwtp0{Strptr1} (id)
        val () = strptr_free (id)
        
      in
        loop (primemap)
      end
      | "n" => let
        val id = req.recv(15)
        val key = g0string2int (id)
        // val () = prerrln! ("Next prime for id:", id)
        val ps = $LinMap.linmap_search_ref<int,intstream> (primemap, key)
      in
        if isneqz (ps) then let
          val sieve = $UN.cptr_get<intstream> (ps)
          val scon = !sieve
          val- ~cons(sh, stail) = scon
          
          val msg = g0int2string (sh)
          val msg = $UN.castvwtp0{string} (msg)
          val () = req.send (msg)
          val msg = $UN.castvwtp0{Strptr1} (msg)
          val () = strptr_free (msg)
          val () = $UN.cptr_set<intstream> (ps, stail)
          val () = req.close
        in
          loop (primemap)
        end
        
        else let
          val () = req.close
          val () = prerrln! ("id: ", id, " not found!")
        in
          loop (primemap)
        end
      end
      | "e" => let
        val id = req.recv (15)
        val key = g0string2int (id)
        val () = req.close
        val opt = $LinMap.linmap_takeout_opt(primemap, key)
        val () = (
          case+ opt of
            | ~None_vt () => ()
            | ~Some_vt (sieve) => stream_vt_free (sieve)
        ): void
      in
        loop (primemap)
      end
      | _ =>> loop (primemap) where {
        val () = prerrln! ("Invalid command: ", msg, " received!")
        val () = req.close
      }
  end
in
  loop (primemap)
end

implement
main0 (argc, argv) = let
  val () = assertloc (argc >= 2)
  val path = argv[1]
  val sock = establish_server (path)
in
  eventloop (sock)
end

(**
  Sieve Code from postiats/doc/EXAMPLE/INTRO/sieve_llazy.dats
  
  Author: Hongwei Xi
  February 2008
*)
typedef N2 = intGte 2

fun
from_con{n:int} (n: int n)
  :<!laz> stream_vt_con (intGte n) =
  stream_vt_cons{intGte(n)}(n, from (n+1))
and from{n:int} (n: int n)
  :<!laz> stream_vt (intGte n) = $ldelay (from_con n)
  
(* ****** ****** *)

fun sieve_con
  (ns: stream_vt N2)
  : stream_vt_con (N2) = let
  val ns_con = !ns
  val-@cons(n, ns2) = ns_con; val p = n
  val ps = sieve
  (
    stream_vt_filter_cloptr (ns2, lam x => g1int_nmod(x, p) > 0)
  ) (* end of [val] *)
  val () = (ns2 := ps)
in
  fold@ ns_con; ns_con
end // end of [sieve_con]

and sieve
  (ns: stream_vt N2): stream_vt (N2) = $ldelay (sieve_con ns, ~ns)
// end of [sieve]

(* ****** ****** *)

implement 
primes (): stream_vt N2 = sieve (from 2)

(* ****** ****** *)
