<?php

/**
  Where the server's socket is.
*/
$SOCKETDIR = "/home/wdblair/work/php/primes/";
$SIEVE = "/home/wdblair/work/php/primes/sieve";
  
$action = $_GET['action'];
  
$socket = socket_create(AF_UNIX, SOCK_STREAM, 0);
      
$socketpath = $SOCKETDIR."sieve.sock";
           
if (socket_connect($socket, $socketpath) === false) {
  $errorcode = socket_last_error();
  $errormsg = socket_strerror($errorcode);

  die("Could not connect to socket: [$errorcode] $errormsg");
}
  
switch ($action) {
      case 'start':
           socket_send ($socket, "s", 1, 0);
           $id = socket_read ($socket, 32);
           echo $id;
        break;
      case 'next':
           /**
                Get the next prime number in the sequence.
           */
           if (!array_key_exists('id', $_GET)) {
              die("No key given");
           }
  
           $id = $_GET['id'];
           if (!is_numeric($id)) {
              die("Invalid id given.");
           }
  
           socket_send ($socket, "n", 1, 0);
           socket_send ($socket, $id, strlen($id), 0);
  
           $prime = socket_read ($socket, 32);
           echo $prime;
           break;
        case 'stop':
           /**
                Stop the process
           */
           if (!array_key_exists('id', $_GET)) {
              die("No key given");
           }
  
           $id = $_GET['id'];
  
           if (!is_numeric($id)) {
              die("Invalid id given.");
           }
  
           socket_send ($socket, "e", 1, 0);
           socket_send ($socket, $id, strlen($id), 0);
           break;
      default:
           die("unknown action given");
}

socket_close($socket);