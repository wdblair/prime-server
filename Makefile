all: sieve

sieve: sieve.dats
	patscc -static -DATS_MEMALLOC_LIBC -D_XOPEN_SOURCE=500 -o sieve $<

.phony: clean

clean:
	rm -f sieve *_dats.c
